import React, { useEffect } from 'react';
import Login from "./containers/Login";
import { ToastContainer } from "react-toastify";
import TodoList from './containers/TodoList';
import { useDispatch, useSelector } from 'react-redux';
import { restoreLogin } from './thunks/authThunks';
import { IRootState } from './store';

const App: React.FC = () => {
    const token = useSelector<IRootState>(state => state.auth.token);
    const dispatch = useDispatch();

    useEffect(() => {
        const token = localStorage.getItem("loginToken");
        if (token) {
            dispatch(restoreLogin(token));
        }
    }, [dispatch]);

    return <>
        <ToastContainer
            position="top-center"
            autoClose={3000}
            hideProgressBar
            draggable={false} />
        {!token && <Login />}
        {!!token && <TodoList />}
    </>
};
export default App;
