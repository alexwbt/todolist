

export const setTokenAndUsername = (token: string, username: string) => ({
    type: "SET_TOKEN_AND_USERNAME" as "SET_TOKEN_AND_USERNAME",
    token, username
});


type ActionCreators = typeof setTokenAndUsername;
type Actions = ReturnType<ActionCreators>;
export default Actions;
