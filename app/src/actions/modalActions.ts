

const showModal = () => ({
    type: "SHOW_MODAL" as "SHOW_MODAL"
});


type ActionCreators = typeof showModal;
type Actions = ReturnType<ActionCreators>;
export default Actions;
