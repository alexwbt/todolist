import { Group, Member, List, Item, Event } from "../reducers/todoReducer";


export const setGroups = (groups: Group[]) => ({
    type: "SET_GROUPS" as "SET_GROUPS",
    groups
});

export const setGroupMembers = (members: Member[]) => ({
    type: "SET_GROUP_MEMBERS" as "SET_GROUP_MEMBERS",
    members
});

export const setTodo = (lists: List[], items: Item[], currentGroup: Group) => ({
    type: "SET_TODO" as "SET_TODO",
    lists, items, currentGroup
});

export const setEvents = (events: Event[], date: string) => ({
    type: "SET_EVENT" as "SET_EVENT",
    events, date
});

export const setCompleteItem = (itemId: number, complete: boolean) => ({
    type: "SET_COMPLETE_ITEM" as "SET_COMPLETE_ITEM",
    itemId, complete
});

export const setHideCompleted = (listId: number, hide: boolean) => ({
    type: "SET_HIDE_COMPLETED" as "SET_HIDE_COMPLETED",
    listId, hide
});


type ActionCreators = typeof setGroups
                    | typeof setGroupMembers
                    | typeof setTodo
                    | typeof setCompleteItem
                    | typeof setHideCompleted
                    | typeof setEvents;
type Actions = ReturnType<ActionCreators>;
export default Actions;
