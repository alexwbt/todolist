import React, { useState } from "react";
import { Event } from "../reducers/todoReducer";
import { Collapse } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";


const eventJsx = [
    (by: string, to: string) => <>"<b>{by}</b>" created this group and named it "<b>{to}</b>".</>,
    (by: string) => <>"<b>{by}</b>" edited this group.</>,
    () => <>Something went wrong here...</>,
    (by: string, to: string) => <>"<b>{by}</b>" added "<b>{to}</b>" to this group.</>,
    (by: string, to: string) => <>"<b>{by}</b>" removed "<b>{to}</b>" from this group.</>,
    (by: string, to: string) => <>"<b>{by}</b>" created a list and named it "<b>{to}</b>".</>,
    (by: string, to: string) => <>"<b>{by}</b>" created an item and titled it "<b>{to}</b>".</>,
    (by: string, to: string) => <>"<b>{by}</b>" edited "<b>{to}</b>" which is a list.</>,
    (by: string, to: string) => <>"<b>{by}</b>" edited "<b>{to}</b>" which is an item.</>,
    (by: string, to: string) => <>"<b>{by}</b>" deleted "<b>{to}</b>" which was a list.</>,
    (by: string, to: string) => <>"<b>{by}</b>" deleted "<b>{to}</b>" which was an item.</>,
    (by: string, to: string) => <>"<b>{by}</b>" completed "<b>{to}</b>".</>,
    (by: string, to: string) => <>"<b>{by}</b>" uncompleted "<b>{to}</b>".</>
];

interface IEventBoxProps {
    key: number;
    events: Event[];
}

const EventBox: React.FC<IEventBoxProps> = (props: IEventBoxProps) => {

    const [open, setOpen] = useState(false);

    return <div className="event-box">
        {
            props.events.length === 1 ? <>
                <span>{props.events[0].created_at}</span>
                {eventJsx[props.events[0].type](props.events[0].by, props.events[0].to)}
            </> : <>
                    <div
                        onClick={() => setOpen(!open)}
                        aria-controls={"events-" + props.key}
                        aria-expanded={open}>
                        <span>{props.events[0].created_at}</span>
                        {eventJsx[props.events[0].type](props.events[0].by, props.events[0].to)}
                        <span className="float-right"><FontAwesomeIcon icon={faEllipsisH} /></span>
                    </div>
                    <Collapse in={open}>
                        <div id={"events-" + props.key}>
                            {
                                props.events.map((e, i) => <>{
                                    i > 0  && <div key={i}>
                                        <span>{e.created_at}</span>
                                        {eventJsx[e.type](e.by, e.to)}
                                    </div>
                                }</>)
                            }
                        </div>
                    </Collapse>
                </>
        }
    </div>
};

export default EventBox;
