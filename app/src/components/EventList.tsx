import React, { ChangeEvent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../store";
import EventBox from "./EventBox";
import { FormControl } from "react-bootstrap";
import { getEvents } from "../thunks/todoThunks";
import { Event } from "../reducers/todoReducer";


const EventList: React.FC = () => {
    const dispatch = useDispatch();
    const { events, eventDate, currentGroupId } = useSelector((state: IRootState) => ({
        events: state.todo.events,
        eventDate: state.todo.eventDate,
        currentGroupId: state.todo.currentGroup.id
    }));

    const groupedEvents: Event[][] = [[]];
    for (let i = 0; i < events.length; i++) {
        if (groupedEvents[0].length <= 0 || events[i].by === groupedEvents[0][0].by) {
            groupedEvents[0].unshift(events[i]);
        } else {
            groupedEvents.unshift([events[i]]);
        }
    }

    return <div className="d-flex flex-column h-100">
        <div className="flex-grow-1" style={{ overflowY: "auto" }}>
            {
                events.length > 0 ? groupedEvents.map((events, i) => <EventBox key={i} events={events} />)
                    : <div className="p-2 text-muted text-center">No events recorded on "{eventDate}"</div>
            }
        </div>
        <div className="p-2 bg-light">
            <FormControl
                type="date"
                value={eventDate}
                className="px-3 rounded-pill"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                    if (currentGroupId)
                        dispatch(getEvents(currentGroupId, e.target.value));
                }} />
        </div>
    </div>
};

export default EventList;
