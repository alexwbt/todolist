import React from "react";
import { Group } from "../reducers/todoReducer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { getGroupTodo, getEvents } from "../thunks/todoThunks";
import { IRootState } from "../store";


interface IGroupBoxProps extends Group { }

const GroupBox: React.FC<IGroupBoxProps> = (props: IGroupBoxProps) => {
    const dispatch = useDispatch();
    const currentGroupId = useSelector((state: IRootState) => state.todo.currentGroup.id);

    return <div className="group-box" onClick={() => {
        dispatch(getGroupTodo(props));
        dispatch(getEvents(props.id, new Date().toISOString().substr(0, 10)));
    }}>
        <span className="group-box-name">{props.name}</span>
        {props.description || "No description"}
        <FontAwesomeIcon
            icon={faAngleRight}
            size="2x"
            color="rgb(240, 240, 240)"
            className={"float-right " + (currentGroupId === props.id && "text-primary")} />
    </div>
};

export default GroupBox;
