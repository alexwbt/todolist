import React from "react";
import { useSelector } from "react-redux";
import { IRootState } from "../store";
import GroupBox from "./GroupBox";
import { FormControl } from "react-bootstrap";


const GroupList: React.FC = () => {

    const { groups } = useSelector((state: IRootState) => ({
        groups: state.todo.groups
    }));

    return <div className="d-flex flex-column h-100">
        <div className="flex-grow-1" style={{ overflowY: "auto" }}>
            {
                groups.map((group, i) => <GroupBox key={i} {...group} />)
            }
        </div>
        <div className="p-2 bg-light">
            <FormControl
                placeholder="Search for group"
                className="px-3 rounded-pill" />
        </div>
    </div>
};

export default GroupList;
