import React, { useState } from "react";
import { Item } from "../reducers/todoReducer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faPlus, faPen, faTrash, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { setCompleteItem, deleteItem } from "../thunks/todoThunks";
import { IRootState } from "../store";
import { Collapse, Button } from "react-bootstrap";


interface IItemBoxProps extends Item { }

const ItemBox: React.FC<IItemBoxProps> = (props: IItemBoxProps) => {
    const dispatch = useDispatch();
    const currentGroupId = useSelector((state: IRootState) => state.todo.currentGroup.id);

    const [showOptions, setShowOptions] = useState(false);

    return <div
        className="item-box"
        title={props.note}>
        <div className="item-box-checkbox">
            <div className="icon" onClick={() => {
                dispatch(setCompleteItem(currentGroupId, props.id, !props.completed));
            }}>
                <FontAwesomeIcon
                    icon={faCheck}
                    className={`border rounded-circle h-100 w-100 p-1 ${props.completed ? "text-success" : "text-white"}`} />
            </div>
        </div>
        <div className="item-box-info">
            <span className="item-box-title">{props.title}</span>
            {props.note}
        </div>
        <div className="options">
            <Collapse in={showOptions}>
                <div id={"item-options-" + props.id} className="flex-grow-1">
                    <Button title="You are working on this" size="sm" variant="outline-info" className="rounded-circle" onClick={() => {

                    }}><FontAwesomeIcon icon={faPlus} /></Button>
                    <Button title="Edit Item" size="sm" variant="outline-warning" className="rounded-circle" onClick={() => {

                    }}><FontAwesomeIcon icon={faPen} /></Button>
                    <Button title="Delete Item" size="sm" variant="outline-danger" className="rounded-circle" onClick={() => {
                        if (window.confirm(`Are you sure you want to delete ${props.title}?`))
                            dispatch(deleteItem(currentGroupId, props.list_id, props.id));
                    }}><FontAwesomeIcon icon={faTrash} /></Button>
                </div>
            </Collapse>
            <Button className="ml-auto"
                aria-controls={"item-options-" + props.id}
                aria-expanded={showOptions} size="sm" variant="light" onClick={() => {
                    setShowOptions(!showOptions);
                }}><FontAwesomeIcon icon={faEllipsisV} /></Button>
        </div>
    </div>
};

export default ItemBox;
