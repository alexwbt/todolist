import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../store";
import { FormControl, Button, Collapse } from "react-bootstrap";
import ListBox from "./ListBox";
import EditModal from "../modals/EditModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash, faPlus, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { deleteGroup } from "../thunks/todoThunks";
import CreateModal from "../modals/CreateModal";


const ItemList: React.FC = () => {
    const dispatch = useDispatch();
    const { lists, items, currentGroup } = useSelector((state: IRootState) => ({
        lists: state.todo.lists,
        items: state.todo.items,
        currentGroup: state.todo.currentGroup
    }));

    const [showEditModal, setShowEditModal] = useState(false);
    const [showCreateList, setShowCreateList] = useState(false);
    const [showOptions, setShowOptions] = useState(false);

    return <div className="d-flex flex-column h-100">
        <EditModal
            type={"Group"}
            show={showEditModal}
            setShow={setShowEditModal} />
        <CreateModal
            type="List"
            show={showCreateList}
            setShow={setShowCreateList} />
        <div className="pl-3 p-1 bg-light shadow-sm d-flex text-right">
            <b>{currentGroup.name}</b>
            {
                !!currentGroup.name && <>
                    {
                        <Collapse in={showOptions}>
                            <div id={"options-" + currentGroup.id} className="flex-grow-1">
                                <Button title="Add List" size="sm" variant="outline-info" className="rounded-circle" onClick={() => {
                                    setShowCreateList(true);
                                }}><FontAwesomeIcon icon={faPlus} /></Button>
                                <Button title="Edit Group" size="sm" variant="outline-warning" className="rounded-circle" onClick={() => {
                                    setShowEditModal(true);
                                }}><FontAwesomeIcon icon={faPen} /></Button>
                                <Button title="Delete Group" size="sm" variant="outline-danger" className="rounded-circle" onClick={() => {
                                    if (window.confirm(`Are you sure you want to delete ${currentGroup.name}?`))
                                        dispatch(deleteGroup(currentGroup.id));
                                }}><FontAwesomeIcon icon={faTrash} /></Button>
                            </div>
                        </Collapse>
                    }
                    <Button className="ml-auto"
                        aria-controls={"options-" + currentGroup.id}
                        aria-expanded={showOptions} size="sm" variant="light" onClick={() => {
                            setShowOptions(!showOptions);
                        }}><FontAwesomeIcon icon={faEllipsisV} /></Button>
                </>
            }
        </div>
        <div className="flex-grow-1" style={{ overflowY: "auto" }}>
            {
                lists.map((list, i) => <ListBox key={i} list={list} items={items.filter(i => i.list_id === list.id)} />)
            }
        </div>
        <div className="p-2 bg-light">
            <FormControl
                placeholder="Search for list"
                className="px-3 rounded-pill" />
        </div>
    </div>
};

export default ItemList;
