import React, { useState } from "react";
import { List, Item } from "../reducers/todoReducer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight, faCaretDown, faPlus, faCheck, faTrash, faPen, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import ItemBox from "./ItemBox";
import { Collapse, Button, ProgressBar } from "react-bootstrap";
import CreateItemModal from "../modals/CreateItemModal";
import { useDispatch, useSelector } from "react-redux";
import { setHideCompleted } from "../actions/todoActions";
import EditModal from "../modals/EditModal";
import { deleteList } from "../thunks/todoThunks";
import { IRootState } from "../store";


interface IListBoxProps {
    list: List;
    items: Item[];
}

const ListBox: React.FC<IListBoxProps> = (props: IListBoxProps) => {
    const dispatch = useDispatch();
    const currentGroupId = useSelector((state: IRootState) => state.todo.currentGroup.id);

    const [open, setOpen] = useState(false);
    const [showCreateItem, setShowCreateItem] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showOptions, setShowOptions] = useState(false);

    const items = props.items.length;
    let count = 0;
    for (let i = 0; i < items; i++) {
        if (props.items[i].completed) {
            count++;
        }
    }
    const progress = Math.floor((count / (items || 1)) * 1000) / 10;

    return <div
        className="border-bottom"
        style={{ overflow: "hidden" }}>
        <EditModal
            type={"List"}
            list={props.list}
            show={showEditModal}
            setShow={setShowEditModal} />
        <CreateItemModal
            listName={props.list.name}
            listId={props.list.id}
            show={showCreateItem}
            setShow={setShowCreateItem} />
        <div
            className="list-box"
            onClick={() => setOpen(!open)}
            aria-controls={"list" + props.list.id}
            aria-expanded={open}
            title={props.list.description}>
            <span className="float-right">
                {props.items.length} items | {progress + "%"}
            </span>
            <span className="list-box-name">
                <FontAwesomeIcon icon={open ? faCaretDown : faCaretRight} className="mr-2" />
                {props.list.name}
            </span>
            {props.list.description}
            <ProgressBar className="progress" now={progress} />
        </div>
        <Collapse in={open}>
            <div id={"list" + props.list.id}>
                {
                    props.list.hideCompleted ?
                        props.items.filter(i => !i.completed).map((item, i) => <ItemBox key={i} {...item} />)
                        :
                        props.items.map((item, i) => <ItemBox key={i} {...item} />)
                }
                <div className="bg-light w-100 text-right p-1 d-flex">
                    <Collapse in={showOptions}>
                        <div id={"list-options-" + props.list.id} className="flex-grow-1">
                            <Button title="Add Todo" size="sm" variant="outline-info" className="mr-1 rounded-circle" onClick={() => {
                                setShowCreateItem(true);
                            }}><FontAwesomeIcon icon={faPlus} /></Button>
                            <Button title="Hide Completed" variant={props.list.hideCompleted ? "success" : "outline-success"}
                                size="sm" className="mr-1 rounded-circle" onClick={() => {
                                    dispatch(setHideCompleted(props.list.id, !props.list.hideCompleted));
                                }}><FontAwesomeIcon icon={faCheck} /></Button>
                            <Button title="Edit List" size="sm" variant="outline-warning" className="mr-1 rounded-circle" onClick={() => {
                                setShowEditModal(true);
                            }}><FontAwesomeIcon icon={faPen} /></Button>
                            <Button title="Delete List" size="sm" variant="outline-danger" className="mr-1 rounded-circle" onClick={() => {
                                if (window.confirm(`Are you sure you want to delete ${props.list.name}?`))
                                    dispatch(deleteList(currentGroupId, props.list.id));
                            }}><FontAwesomeIcon icon={faTrash} /></Button>
                        </div>
                    </Collapse>
                    <Button className="ml-auto"
                        aria-controls={"list-options-" + props.list.id}
                        aria-expanded={showOptions} size="sm" variant="light" onClick={() => {
                            setShowOptions(!showOptions);
                        }}><FontAwesomeIcon icon={faEllipsisV} /></Button>
                </div>
            </div>
        </Collapse>
    </div>
};

export default ListBox;
