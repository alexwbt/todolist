import React, { useState } from "react";
import { Navbar, Nav, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { setTokenAndUsername } from "../actions/authActions";
import CreateModal from "../modals/CreateModal";
import { IRootState } from "../store";


const Menu: React.FC = () => {
    const dispatch = useDispatch();
    const username = useSelector((state: IRootState) => state.auth.username);

    const [showCreateGroup, setShowCreateGroup] = useState(false);

    return <Navbar bg="light" expand="lg" className="py-0">
        <CreateModal
            type="Group"
            show={showCreateGroup}
            setShow={setShowCreateGroup} />
        <Navbar.Brand href="#home">TodoList</Navbar.Brand>
        <Navbar.Toggle className="border-0" aria-controls="navbar" />
        <Navbar.Collapse id="navbar">
            <Nav className="mr-auto">
                <Button
                    variant={"light"}
                    className="rounded-0"
                    onClick={() => setShowCreateGroup(true)}>
                    New Group
                </Button>
            </Nav>
            <span className="m-0 px-2 bg-light">Logged In as <b>{username}</b></span>
            <Button variant="link" onClick={() => {
                localStorage.setItem("loginToken", "");
                dispatch(setTokenAndUsername("", ""));
            }}>Logout</Button>
        </Navbar.Collapse>
    </Navbar>
};

export default Menu;
