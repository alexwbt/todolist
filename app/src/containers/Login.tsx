import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import GoogleLogin from "react-google-login";
import { useDispatch } from "react-redux";
import { login } from "../thunks/authThunks";


const Login: React.FC = () => {
    const dispatch = useDispatch();

    const [height, setHeight] = useState(0);
    useEffect(() => {
        const resetHeight = () => {
            setHeight(window.innerHeight);
        };
        window.addEventListener("resize", resetHeight);
        resetHeight();

        return () => {
            window.removeEventListener("resize", resetHeight);
        };
    }, []);

    const googleResponse = async (response: any) => {
        dispatch(login(response.tokenObj.access_token));
    };

    return <Container fluid className="bg-light">
        <Row className="justify-content-center align-items-center" style={{ height }}>
            <Col lg={3} className="text-center">
                <div className="shadow py-4 bg-white" style={{ borderRadius: 10 }}>
                    <h1 className="mb-3">TodoList</h1>
                    <GoogleLogin
                        clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID || ""}
                        buttonText="Login With Google"
                        onSuccess={googleResponse}
                        onFailure={googleResponse} />
                </div>
            </Col>
        </Row>
    </Container>
};
export default Login;
