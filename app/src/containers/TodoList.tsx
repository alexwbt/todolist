import React, { useEffect } from "react";
import Menu from "../components/Menu";
import { useDispatch, useSelector } from "react-redux";
import { getGroups } from "../thunks/todoThunks";
import GroupList from "../components/GroupList";
import { Row, Container, Col } from "react-bootstrap";
import { IRootState } from "../store";
import ItemList from "../components/ItemList";
import EventList from "../components/EventList";

import "../styles/TodoList.scss";


const TodoList: React.FC = () => {
    const dispatch = useDispatch();
    const tab = useSelector((state: IRootState) => state.todo.tab);

    useEffect(() => {
        dispatch(getGroups());
    }, [dispatch]);

    return <div className="d-flex flex-column vh-100">
        <Container fluid className="flex-grow-1" style={{ overflow: "hidden" }}>
            <Row className="h-100">
                <Col lg={3} className={`${tab !== 0 && "d-none"} d-lg-block border p-0 h-100`}>
                    <GroupList />
                </Col>
                <Col lg={5} className={`${tab !== 1 && "d-none"} d-lg-block border p-0 h-100`}>
                    <ItemList />
                </Col>
                <Col lg={4} className={`${tab !== 2 && "d-none"} d-lg-block border p-0 h-100`}>
                    <EventList />
                </Col>
            </Row>
        </Container>
        <Menu />
    </div>
};

export default TodoList;
