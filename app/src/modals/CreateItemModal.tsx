import React, { useState, ChangeEvent, FormEvent } from "react";
import { Modal, Button, FormControl, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { createItem } from "../thunks/todoThunks";
import { IRootState } from "../store";


interface ICreateItemModalProps {
    show: boolean;
    setShow: (show: boolean) => void;
    listName: string;
    listId: number;
}

const CreateItemModal: React.FC<ICreateItemModalProps> = (props: ICreateItemModalProps) => {
    const [title, setTitle] = useState("");
    const [note, setNote] = useState("");
    const [color, setColor] = useState(0);
    const [due, setDue] = useState("");

    const dispatch = useDispatch();
    const currentGroup = useSelector((state: IRootState) => state.todo.currentGroup);

    return <Modal show={props.show} onHide={() => props.setShow(false)}>
        <Modal.Header>
            <Modal.Title>New Todo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => {
                e.preventDefault();
                dispatch(createItem(currentGroup.id, props.listId, title,
                    note ? note : undefined,
                    color ? color : undefined,
                    due ? due : undefined));
            }}>
                <FormControl
                    className="mb-3"
                    placeholder="New Todo Title"
                    value={title}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setTitle(e.target.value.substr(0, 40));
                    }}
                    required />
                <FormControl
                    className="mb-3"
                    placeholder="Note"
                    value={note}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setNote(e.target.value);
                    }} />
                <FormControl
                    className="mb-3"
                    as="select"
                    value={color + ""}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setColor(parseInt(e.target.value));
                    }}>
                    <option value={0} className="text-muted">No Color</option>
                    <option value={1} className="text-dark">black</option>
                    <option value={2} className="text-primary">blue</option>
                    <option value={3} className="text-success">green</option>
                    <option value={4} className="text-warning">yellow</option>
                    <option value={5} className="text-danger">red</option>
                </FormControl>
                <FormControl
                    type="date"
                    value={due}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setDue(e.target.value);
                    }} />
                <div className="py-2 text-muted">Adding to "{props.listName}" in "{currentGroup.name}"</div>
                <div>
                    <Button
                        className="mr-2"
                        variant="secondary"
                        size="sm"
                        onClick={() => props.setShow(false)}>
                        Cancel
                    </Button>
                    <input
                        className="btn btn-primary btn-sm"
                        type="submit"
                        value="Add" />
                </div>
            </Form>
        </Modal.Body>
    </Modal>;
};

export default CreateItemModal;
