import React, { useState, ChangeEvent, FormEvent } from "react";
import { Modal, Button, FormControl, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { createGroup, createList } from "../thunks/todoThunks";
import { IRootState } from "../store";


interface ICreateModalProps {
    show: boolean;
    setShow: (show: boolean) => void;
    type: "Group" | "List";
}

const CreateModal: React.FC<ICreateModalProps> = (props: ICreateModalProps) => {
    const dispatch = useDispatch();
    const currentGroup = useSelector((state: IRootState) => state.todo.currentGroup);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");

    return <Modal show={props.show} onHide={() => props.setShow(false)}>
        <Modal.Header>
            <Modal.Title>New {props.type}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => {
                e.preventDefault();
                switch (props.type) {
                    case "Group": dispatch(createGroup(name, description ? description : undefined)); break;
                    case "List": dispatch(createList(currentGroup.id, name, description ? description : undefined));
                }
            }}>
                <FormControl
                    className="mb-3"
                    placeholder={`New ${props.type} Name`}
                    value={name}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setName(e.target.value.substr(0, 40));
                    }}
                    required />
                <FormControl
                    className="mb-3"
                    placeholder={`${props.type} Description`}
                    value={description}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setDescription(e.target.value);
                    }} />
                {
                    props.type === "List" &&
                    <div className="py-2 text-muted">Adding to "{currentGroup.name}"</div>
                }
                <div>
                    <Button
                        className="mr-2"
                        variant="secondary"
                        size="sm"
                        onClick={() => props.setShow(false)}>
                        Cancel
                    </Button>
                    <input
                        className="btn btn-primary btn-sm"
                        type="submit"
                        value="Create" />
                </div>
            </Form>
        </Modal.Body>
    </Modal>;
};

export default CreateModal;
