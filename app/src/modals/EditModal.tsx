import React, { useState, ChangeEvent, FormEvent } from "react";
import { Modal, Button, FormControl, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { editGroup, editList } from "../thunks/todoThunks";
import { IRootState } from "../store";
import { List } from "../reducers/todoReducer";


interface IEditModalProps {
    show: boolean;
    setShow: (show: boolean) => void;
    type: "Group" | "List";
    list?: List;
}

const EditModal: React.FC<IEditModalProps> = (props: IEditModalProps) => {
    const dispatch = useDispatch();
    const currentGroup = useSelector((state: IRootState) => state.todo.currentGroup);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");

    if (props.show && !name) {
        if (props.type === "Group") {
            setName(currentGroup.name);
            setDescription(currentGroup.description || "");
        } else if (props.list) {
            setName(props.list.name);
            setDescription(props.list.description || "");
        }
    }

    return <Modal show={props.show} onHide={() => props.setShow(false)}>
        <Modal.Header>
            <Modal.Title>Edit {props.type}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
            <Form onSubmit={(e: FormEvent<HTMLFormElement>) => {
                e.preventDefault();
                switch (props.type) {
                    case "Group": dispatch(editGroup(currentGroup.id, name, description ? description : undefined)); break;
                    case "List": dispatch(editList(currentGroup.id, props.list?.id || 0, name, description ? description : undefined));
                }
            }}>
                <FormControl
                    className="mb-3"
                    value={name}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setName(e.target.value.substr(0, 40));
                    }}
                    required />
                <FormControl
                    className="mb-3"
                    value={description}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setDescription(e.target.value);
                    }} />
                {
                    <div className="py-2 text-muted">Editing{props.type === "List" ? ` "${props.list?.name}" in` : ""} "{currentGroup.name}"</div>
                }
                <div>
                    <Button
                        className="mr-2"
                        variant="secondary"
                        size="sm"
                        onClick={() => props.setShow(false)}>
                        Cancel
                    </Button>
                    <input
                        className="btn btn-primary btn-sm"
                        type="submit"
                        value="Edit" />
                </div>
            </Form>
        </Modal.Body>
    </Modal>;
};

export default EditModal;
