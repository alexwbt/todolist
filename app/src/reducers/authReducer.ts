import AuthAction from "../actions/authActions";


export interface IAuthState {
    token: string;
    username: string;
}

const initialState = {
    token: localStorage.getItem("loginToken") || "",
    username: ""
};

const authReducer = (state: IAuthState = initialState, action: AuthAction) => {
    switch (action.type) {
        case "SET_TOKEN_AND_USERNAME":
            return {
                token: action.token,
                username: action.username
            };
        default:
            return state;
    }
};

export default authReducer;
