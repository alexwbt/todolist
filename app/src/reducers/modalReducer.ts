import ModalAction from "../actions/modalActions";
import { Group, List, Item } from "./todoReducer";


enum ModalType {
    CREATE_GROUP,
    EDIT_GROUP,
    CREATE_LIST,
    EDIT_LIST,
    CREATE_ITEM,
    EDIT_ITEM
}

export interface IModalState {
    show: boolean;
    type: ModalType;
    group?: Group;
    list?: List;
    item?: Item;
}

const initialState = {
    show: false,
    type: -1
};


const modalReducer = (state: IModalState = initialState, action: ModalAction) => {
    switch (action.type) {

    }
};

export default modalReducer;
