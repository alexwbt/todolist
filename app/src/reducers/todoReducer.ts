import TodoAction from "../actions/todoActions";

export interface Group {
    id: number;
    name: string;
    description: string;
}

export interface Member {
    id: number;
    name: string;
    gmail: string;
    profile: string;
}

export interface List {
    id: number;
    name: string;
    description: string;
    hideCompleted: boolean;
}

export interface Item {
    id: number;
    list_id: number;
    title: string;
    note: string;
    color: number;
    due: string;
    completed: boolean;
}

export interface Event {
    type: number;
    by: string;
    to: string;
    created_at: string;
}

export enum Tab {
    GROUP_LIST,
    ITEM_LIST,
    INFO
}

export interface ITodoState {
    groups: Group[];
    members: Member[];
    lists: List[];
    items: Item[];

    events: Event[];
    eventDate: string;

    currentGroup: Group;
    tab: Tab;
}

const initialState = {
    groups: [],
    members: [],
    lists: [],
    items: [],

    events: [],
    eventDate: new Date().toISOString().substr(0, 10),

    currentGroup: {
        id: 0,
        name: "",
        description: ""
    },
    tab: 0
};

const todoReducer = (state: ITodoState = initialState, action: TodoAction) => {
    switch (action.type) {
        case "SET_GROUPS":
            return {
                ...state,
                groups: action.groups
            };
        case "SET_GROUP_MEMBERS":
            return {
                ...state,
                members: action.members
            };
        case "SET_TODO":
            return {
                ...state,
                lists: action.lists.map(list => ({ ...list, hideCompleted: false })),
                items: action.items,
                currentGroup: action.currentGroup
            };
        case "SET_EVENT":
            return {
                ...state,
                events: action.events.map(e => ({
                    ...e,
                    created_at: new Date(e.created_at).toString().substr(16, 8)
                })),
                eventDate: action.date
            };
        case "SET_COMPLETE_ITEM":
            return {
                ...state,
                items: state.items.map(item => {
                    if (item.id === action.itemId) {
                        return {
                            ...item,
                            completed: action.complete
                        };
                    }
                    return item;
                })
            };
        case "SET_HIDE_COMPLETED":
            return {
                ...state,
                lists: state.lists.map(list => {
                    if (list.id === action.listId) {
                        return {
                            ...list,
                            hideCompleted: action.hide
                        };
                    }
                    return list;
                })
            };
        default:
            return state;
    }
};

export default todoReducer;
