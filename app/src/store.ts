import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk, { ThunkDispatch as ReduxThunkDispatch } from "redux-thunk";
import authReducer, { IAuthState } from "./reducers/authReducer";
import todoReducer, { ITodoState } from "./reducers/todoReducer";
import AuthAction from "./actions/authActions";
import TodoAction from "./actions/todoActions";


export interface IRootState {
    auth: IAuthState;
    todo: ITodoState;
}

type RootAction = AuthAction | TodoAction;

const rootReducer = combineReducers<IRootState>({
    auth: authReducer,
    todo: todoReducer
});


export type ThunkDispatch = ReduxThunkDispatch<IRootState, null, RootAction>;

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState, RootAction, {}, {}>(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));
