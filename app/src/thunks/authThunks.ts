import { Dispatch } from "react";
import AuthAction, { setTokenAndUsername } from "../actions/authActions";
import { toast } from "react-toastify";


const { REACT_APP_API_SERVER } = process.env;

export function login(accessToken: string) {
    return async (dispatch: Dispatch<AuthAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/user/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ accessToken })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                localStorage.setItem("loginToken", result.token);
                dispatch(setTokenAndUsername(result.token, result.username));
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to login");
        }
    };
}

export function restoreLogin(token: string) {
    return async (dispatch: Dispatch<AuthAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/user/restore`, {
                method: "POST",
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                localStorage.setItem("loginToken", result.token);
                dispatch(setTokenAndUsername(result.token, result.username));
            } else {
                localStorage.setItem("loginToken", "");
                dispatch(setTokenAndUsername("", ""));
                toast.error(result.message);
            }
        } catch (err) {
            localStorage.setItem("loginToken", "");
            dispatch(setTokenAndUsername("", ""));
            toast.error("Failed to restore access");
        }
    };
}
