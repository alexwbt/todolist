import { Dispatch } from "react";
import TodoAction, { setGroupMembers, setTodo, setCompleteItem as setComplete, setEvents } from "../actions/todoActions"
import { toast } from "react-toastify";
import { setGroups } from "../actions/todoActions";
import { Group } from "../reducers/todoReducer";


const { REACT_APP_API_SERVER } = process.env;

export function setCompleteItem(groupId: number, itemId: number, complete: boolean) {
    return async (dispatch: Dispatch<TodoAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/${complete ? "complete" : "uncomplete"}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, itemId })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                dispatch(setComplete(itemId, complete));
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to get groups");
        }
    };
}

export function getGroups() {
    return async (dispatch: Dispatch<TodoAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/group`, {
                headers: { "Authorization": `Bearer ${localStorage.getItem("loginToken")}` }
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                dispatch(setGroups(result.groups));
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to get groups");
        }
    };
}

export function getGroupMembers(groupId: number) {
    return async (dispatch: Dispatch<TodoAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/member/${groupId}`, {
                headers: { "Authorization": `Bearer ${localStorage.getItem("loginToken")}` }
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                dispatch(setGroupMembers(result.members));
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to get group members");
        }
    };
}

export function getGroupTodo(group: Group) {
    return async (dispatch: Dispatch<TodoAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/${group.id}`, {
                headers: { "Authorization": `Bearer ${localStorage.getItem("loginToken")}` }
            });
            const result = await res.json();

            if (res.status === 200 && result.success) {
                dispatch(setTodo(result.lists, result.items, group));
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to get todo list");
        }
    };
}

export function getEvents(group_id: number, date: string) {
    return async (dispatch: Dispatch<TodoAction>) => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/event/${group_id}/${date}`, {
                headers: { "Authorization": `Bearer ${localStorage.getItem("loginToken")}` }
            });
            const result = await res.json();

            if (res.status === 200 && result.success) {
                console.log(result);
                dispatch(setEvents(result.events, date));
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to get events");
        }
    };
}

export function createGroup(name: string, description?: string) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/group`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ name, description })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully created new group");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to create group");
        }
    };
}

export function editGroup(groupId: number, name: string, description?: string) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/group`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, name, description })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully edited group");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to edit group");
        }
    };
}

export function deleteGroup(groupId: number) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/group`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully deleted group");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to delete group");
        }
    };
}

export function addMember(groupId: number, gmail: string) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/member`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, gmail })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully added member");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to add member");
        }
    };
}

export function removeMember(groupId: number, userId: number) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/member`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, userId })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully deleted member");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to delete member");
        }
    };
}

export function createList(groupId: number, name: string, description?: string) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/list`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, name, description })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully created list");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to create list");
        }
    };
}

export function editList(groupId: number, listId: number, name: string, description?: string) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/list`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, listId, name, description })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully edited list");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to edit list");
        }
    };
}

export function deleteList(groupId: number, listId: number) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/list`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, listId })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully deleted list");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to delete list");
        }
    };
}

export function createItem(groupId: number, listId: number, title: string, note?: string, color?: number, due?: string) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/item`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, listId, title, note, color, due })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully created item");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to create item");
        }
    };
}

export function editItem(groupId: number, itemId: number, title: string, note?: string, color?: number, due?: Date) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/item`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, itemId, title, note, color, due })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully edited item");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to edit item");
        }
    };
}

export function deleteItem(groupId: number, listId: number, itemId: number) {
    return async () => {
        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/todo/item`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("loginToken")}`
                },
                body: JSON.stringify({ groupId, listId, itemId })
            });
            const result = await res.json();
            if (res.status === 200 && result.success) {
                toast.success("Successfully deleted item");
            } else {
                toast.error(result.message);
            }
        } catch (err) {
            toast.error("Failed to delete item");
        }
    };
}
