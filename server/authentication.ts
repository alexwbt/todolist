import * as passport from "passport";
import * as passportJWT from "passport-jwt";
import * as jwtSimple from "jwt-simple";
import { userService } from "./main";


const jwt = {
    jwtSecret: "ivGpUhNzjV4BWuLuKShoE9nFrmXabqLQBMtdilVrjupf51HgXgs7qVzeVIcG",
    jwtSession: { session: false }
};

passport.use(new passportJWT.Strategy({
    secretOrKey: jwt.jwtSecret,
    jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken()
}, async (payload, done) => {
    try {
        const user = await userService.getUserWithGmail(payload.gmail);
        return done(null, user);
    } catch (err) {
        return done(err, null);
    }
}));

export const getToken = (userId: number, gmail: string) =>
    jwtSimple.encode({ id: userId, gmail }, jwt.jwtSecret);

export const isLoggedIn = passport.authenticate('jwt', { session: false });