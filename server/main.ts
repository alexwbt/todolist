import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as Knex from "knex";
import UserService from "./services/UserService";
import UserRouter from "./routers/UserRouter";
import TodoService from "./services/TodoService";
import TodoRouter from "./routers/TodoRouter";


const app = express();
const PORT = 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());

const knexConfig = require('./knexfile');
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

export const userService = new UserService();
const userRouter = new UserRouter();
app.use("/user", userRouter.Router);

export const todoService = new TodoService();
const todoRouter = new TodoRouter();
app.use("/todo", todoRouter.Router);

app.listen(PORT, () => {
    console.log("Running express server on port " + PORT);
});