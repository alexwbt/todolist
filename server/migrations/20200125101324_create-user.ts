import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable("user", table => {
        table.increments();
        table.string("name", 40).notNullable();
        table.string("gmail").notNullable().unique();
        table.string("profile").notNullable();
    });
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTable("user");
}

