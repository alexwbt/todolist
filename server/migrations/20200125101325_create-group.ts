import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable("group", table => {
        table.increments();
        table.string("name", 40).notNullable();
        table.string("description").defaultTo(null);
    });
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTable("group");
}

