import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable("list", table => {
        table.increments();
        table.integer("group_id").unsigned().notNullable();
        table.foreign("group_id").references("group.id");
        table.string("name", 40).notNullable();
        table.string("description").defaultTo(null);
    });
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTable("list");
}

