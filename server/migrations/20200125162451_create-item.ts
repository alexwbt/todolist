import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable("item", table => {
        table.increments();
        table.integer("list_id").unsigned().notNullable();
        table.foreign("list_id").references("list.id");
        table.string("title", 40).notNullable();
        table.string("note").defaultTo(null);
        table.integer("color").unsigned().defaultTo(0);
        table.date("due").defaultTo(null);
        table.boolean("completed").defaultTo(false);
    });
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTable("item");
}

