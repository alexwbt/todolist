import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable("member", table => {
        table.integer("group_id").unsigned().notNullable();
        table.foreign("group_id").references("group.id");
        table.integer("user_id").unsigned().notNullable();
        table.foreign("user_id").references("user.id");
        table.unique(["group_id", "user_id"]);
    });
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTable("member");
}

