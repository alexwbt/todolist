import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable("event", table => {
        table.integer("group_id").unsigned().notNullable();
        table.foreign("group_id").references("group.id");
        table.integer("type").unsigned().notNullable();
        table.string("by", 40).notNullable();
        table.string("to", 40).defaultTo(null);
        table.timestamp('created_at').defaultTo(knex.fn.now());
    });
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTable("event");
}

