import { Router, Request, Response, NextFunction } from "express";


export default class ExpressRouter {

    protected router: Router;

    constructor() {
        this.router = Router();
    }

    get Router() {
        return this.router;
    }

    protected catcher(routerFunction: (req: Request, res: Response, next?: NextFunction) => Promise<string | void>) {
        return async (req: Request, res: Response, next?: NextFunction) => {
            try {
                const message = await routerFunction(req, res, next);
                if (message && !res.headersSent) {
                    res.status(403).json({
                        success: false,
                        message
                    });
                    return;
                }
            } catch (err) {
                console.log(err.message);
                if (!res.headersSent) {
                    res.status(500).json({
                        success: false,
                        message: "Internal Server Error"
                    });
                }
            }
            if (!res.headersSent) {
                console.log(req.originalUrl + " doesn't have a response.");
                res.status(500).json({
                    success: false,
                    message: "No Response"
                });
            }
        };
    }

}