import { Request, Response } from "express";
import { isLoggedIn } from "../authentication";
import { todoService, userService } from "../main";
import ExpressRouter from "./ExpressRouter";

interface Body {
    userId: number;
    gmail: string;
    groupId: number;
    listId: number;
    itemId: number;
    name: string;
    description?: string;
    title: string;
    note?: string;
    due?: string;
    color?: number;
}

interface User {
    id: number;
    name: string;
}

export default class TodoRouter extends ExpressRouter {

    constructor() {
        super();
        this.router.use(isLoggedIn);
        this.router.get("/group", this.catcher(this.getGroups));
        this.router.get("/member/:groupId", this.catcher(this.getGroupMembers));
        this.router.get("/event/:groupId/:date", this.catcher(this.getEvents));
        this.router.get("/:groupId", this.catcher(this.getGroupTodo));

        this.router.post("/complete", this.catcher(this.completeItem));
        this.router.post("/uncomplete", this.catcher(this.uncompleteItem));
        this.router.post("/group", this.catcher(this.createGroup));
        this.router.post("/member", this.catcher(this.addMember));
        this.router.post("/list", this.catcher(this.createList));
        this.router.post("/item", this.catcher(this.createItem));

        this.router.put("/group", this.catcher(this.editGroup));
        this.router.put("/list", this.catcher(this.editList));
        this.router.put("/item", this.catcher(this.editItem));

        this.router.delete("/group", this.catcher(this.deleteGroup));
        this.router.delete("/member", this.catcher(this.removeMember));
        this.router.delete("/list", this.catcher(this.deleteList));
        this.router.delete("/item", this.catcher(this.deleteItem));
    }

    private completeItem = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, itemId } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!itemId) return "Item ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.itemIsInGroup(groupId, itemId))
            return "Permission Denied";

        await todoService.completeItem(user.name, groupId, itemId);
        res.status(200).json({ success: true });
    };

    private uncompleteItem = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, itemId } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!itemId) return "Item ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.itemIsInGroup(groupId, itemId))
            return "Permission Denied";

        await todoService.uncompleteItem(user.name, groupId, itemId);
        res.status(200).json({ success: true });
    };

    private getGroups = async (req: Request, res: Response): Promise<string | void> => {
        res.status(200).json({
            success: true,
            groups: await todoService.getGroups((req.user as any).id)
        });
    };

    private getGroupMembers = async (req: Request, res: Response): Promise<string | void> => {
        const groupId = parseInt(req.params.groupId);
        if (!groupId) return "Group ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        const memberIds = await todoService.getMembers(groupId);
        res.status(200).json({
            success: true,
            members: await userService.getMembers(memberIds.map(m => m.id))
        });
    };

    private getGroupTodo = async (req: Request, res: Response): Promise<string | void> => {
        const groupId = parseInt(req.params.groupId);
        if (!groupId) return "Group ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        const lists = await todoService.getLists(groupId);
        const items = await todoService.getItems(lists.map(list => list.id));
        res.status(200).json({ success: true, lists, items });
    };

    private getEvents = async (req: Request, res: Response): Promise<string | void> => {
        const groupId = parseInt(req.params.groupId);
        const date = req.params.date;
        if (!groupId) return "Group ID Required";
        if (!date) return "Date Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        res.status(200).json({
            success: true,
            events: await todoService.getEvents(groupId, date)
        });
    };

    private createGroup = async (req: Request, res: Response): Promise<string | void> => {
        const { name, description } = req.body as Body;
        if (!name) return "Group Name Required";
        if (name.length > 40) return "Name Too Long";

        const user = req.user as User;

        await todoService.createGroup(user.id, user.name, name, description);
        res.status(200).json({ success: true });
    };

    private editGroup = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, name, description } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!name) return "Group Name Required";
        if (name.length > 40) return "Name Too Long";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        await todoService.editGroup(user.name, groupId, name, description);
        res.status(200).json({ success: true });
    };

    private deleteGroup = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId } = req.body as Body;
        if (!groupId) return "Group ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        await todoService.deleteGroup(groupId);
        res.status(200).json({ success: true });
    };

    private addMember = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, gmail } = req.body as Body;
        if (!groupId) return "Group ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        const member = await userService.getUserWithGmail(gmail);
        if (!user) return "Unknown User";

        await todoService.addMember(groupId, member.id, user.name, member.name);
        res.status(200).json({ success: true });
    };

    private removeMember = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, userId } = req.body as Body;
        if (!groupId) return "Group ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        const member = await userService.getUserWithId(userId);
        if (!member || !await todoService.memberExists(groupId, member.id)) return "Unknown Member";

        await todoService.removeMember(groupId, member.id, user.name, member.name);
        res.status(200).json({ success: true });
    };

    private createList = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, name, description } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!name) return "List Name Required";
        if (name.length > 40) return "Name Too Long";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)) return "Permission Denied";

        await todoService.createList(user.name, groupId, name, description);
        res.status(200).json({ success: true });
    };

    private editList = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, listId, name, description } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!listId) return "List ID Required";
        if (!name) return "List Name Required";
        if (name.length > 40) return "Name Too Long";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.listIsInGroup(groupId, listId))
            return "Permission Denied";

        await todoService.editList(user.name, groupId, listId, name, description);
        res.status(200).json({ success: true });
    };

    private deleteList = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, listId } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!listId) return "List ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.listIsInGroup(groupId, listId))
            return "Permission Denied";

        await todoService.deleteList(user.name, groupId, listId);
        res.status(200).json({ success: true });
    };

    private createItem = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, listId, title, note, color, due } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!title) return "Item Title Required";
        if (title.length > 40) return "Title Too Long";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.listIsInGroup(groupId, listId))
            return "Permission Denied";

        await todoService.createItem(user.name, groupId, listId, title, note, color, due);
        res.status(200).json({ success: true });
    };

    private editItem = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, itemId, title, note, color, due } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!itemId) return "Item ID Required";
        if (!title) return "Item Title Required";
        if (title.length > 40) return "Title Too Long";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.itemIsInGroup(groupId, itemId))
            return "Permission Denied";

        await todoService.editItem(user.name, groupId, itemId, title, note, color, due);
        res.status(200).json({ success: true });
    };

    private deleteItem = async (req: Request, res: Response): Promise<string | void> => {
        const { groupId, listId, itemId } = req.body as Body;
        if (!groupId) return "Group ID Required";
        if (!listId) return "List ID Required";
        if (!itemId) return "Item ID Required";

        const user = req.user as User;
        if (!await todoService.memberExists(groupId, user.id)
            || !await todoService.listIsInGroup(groupId, listId)
            || !await todoService.itemIsInGroup(groupId, itemId))
            return "Permission Denied";

        await todoService.deleteItem(user.name, groupId, itemId, listId);
        res.status(200).json({ success: true });
    };

}
