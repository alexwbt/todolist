import { Request, Response } from "express";
import fetch from "node-fetch";
import { userService } from "../main";
import { getToken, isLoggedIn } from "../authentication";
import ExpressRouter from "./ExpressRouter";

export default class UserRouter extends ExpressRouter {

    constructor() {
        super();
        this.router.post("/login", this.catcher(this.login));
        this.router.post("/restore", isLoggedIn, this.catcher(this.restore));
    }

    private login = async (req: Request, res: Response): Promise<string | void> => {
        const { accessToken } = req.body;
        if (!accessToken) return "Access Token Required";

        const response = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            headers: { "Authorization": `Bearer ${accessToken}` }
        });
        const result = await response.json();
        if (result.error) return "Invalid Access Token";
        if (!result.verified_email) return "Unverified Email";
        if (!result.name) return "Missing Name";
        if (!result.email) return "Missing Email";
        if (!result.picture) return "Missing Picture";

        let user: any;
        user = await userService.getUserWithGmail(result.email);
        if (!user) {
            await userService.addUser(result.name, result.email, result.picture);
            user = await userService.getUserWithGmail(result.email);
        }

        res.status(200).json({
            success: true,
            username: user.name,
            token: getToken(user.id, user.gmail)
        });
    };

    private restore = async (req: Request, res: Response) => {
        const user = req.user as any;
        res.status(200).json({
            success: true,
            username: user.name,
            token: getToken(user.id, user.gmail)
        });
    };

}
