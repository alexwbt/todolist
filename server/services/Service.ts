import Knex = require("knex");
import { knex } from "../main";


export default class Service {

    protected async transaction(service: (trx: Knex.Transaction) => any) {
        const trx = await knex.transaction();
        try {
            const value = await service(trx);
            trx.commit();
            return value;
        } catch (err) {
            await trx.rollback();
            throw new Error(err);
        }
    }

}
