import Service from "./Service";
import Knex = require("knex");
import { knex } from "../main";


enum EventType {
    CREATE_GROUP,
    EDIT_GROUP,
    DELETE_GROUP,
    ADD_MEMBER,
    REMOVE_MEMBER,
    CREATE_LIST,
    CREATE_ITEM,
    EDIT_LIST,
    EDIT_ITEM,
    DELETE_LIST,
    DELETE_ITEM,
    COMPLETE_ITEM,
    UNCOMPLETE_ITEM
}

export default class TodoService extends Service {

    private async recordEvent(trx: Knex.Transaction, group_id: number, type: EventType, by: string, to?: string) {
        return trx("event").insert({ group_id, type, by, to });
    }

    async createGroup(user_id: number, username: string, name: string, description?: string) {
        return this.transaction(async trx => {
            const group = await trx("group").insert({ name, description }).returning("id");
            await trx("member").insert({ group_id: group[0], user_id });
            await this.recordEvent(trx, group[0], EventType.CREATE_GROUP, username, name);
        });
    }

    async editGroup(username: string, id: number, name: string, description?: string) {
        return this.transaction(async trx => {
            await trx("group").where({ id }).update({ name, description });
            await this.recordEvent(trx, id, EventType.EDIT_GROUP, username, name);
        });
    }

    async deleteGroup(group_id: number) {
        return this.transaction(async trx => {
            const lists = await trx("list").select("id").where({ group_id });
            await trx("item").whereIn("list_id", lists.map(i => i.id)).del();
            await trx("list").where({ group_id }).del();
            await trx("member").where({ group_id }).del();
            await trx("event").where({ group_id }).del();
            await trx("group").where({ id: group_id }).del();
        });
    }

    async addMember(group_id: number, user_id: number, byUsername: string, toUsername: string) {
        return this.transaction(async trx => {
            await trx("member").insert({ group_id, user_id });
            await this.recordEvent(trx, group_id, EventType.ADD_MEMBER, byUsername, toUsername);
        });
    }

    async removeMember(group_id: number, user_id: number, byUsername: string, toUsername: string) {
        return this.transaction(async trx => {
            await trx("member").where({ group_id, user_id }).del();
            await this.recordEvent(trx, group_id, EventType.REMOVE_MEMBER, byUsername, toUsername);
        });
    }

    async createList(username: string, group_id: number, name: string, description?: string) {
        return this.transaction(async trx => {
            await trx("list").insert({ group_id, name, description });
            await this.recordEvent(trx, group_id, EventType.CREATE_LIST, username, name);
        });
    }

    async editList(username: string, group_id: number, id: number, name: string, description?: string) {
        return this.transaction(async trx => {
            await trx("list").where({ id, group_id }).update({ name, description });
            await this.recordEvent(trx, group_id, EventType.EDIT_LIST, username, name);
        });
    }

    async deleteList(username: string, group_id: number, id: number) {
        return this.transaction(async trx => {
            const list = await trx("list").select("name").where(id, group_id);
            await trx("item").where({ list_id: id }).del();
            await trx("list").where({ id, group_id }).del();
            await this.recordEvent(trx, group_id, EventType.DELETE_LIST, username, list[0].name);
        });
    }

    async createItem(username: string, group_id: number, list_id: number, title: string, note?: string, color?: number, due?: string) {
        return this.transaction(async trx => {
            await trx("item").insert({ list_id, title, note, color, due });
            await this.recordEvent(trx, group_id, EventType.CREATE_ITEM, username, title);
        });
    }

    async editItem(username: string, group_id: number, id: number, title: string, note?: string, color?: number, due?: string) {
        return this.transaction(async trx => {
            await trx("item").where({ id }).update({ title, note, color, due });
            await this.recordEvent(trx, group_id, EventType.EDIT_ITEM, username, title);
        });
    }

    async deleteItem(username: string, group_id: number, id: number, list_id: number) {
        return this.transaction(async trx => {
            const item = await trx("item").select("title").where({ id, list_id });
            await trx("item").where({ id, list_id }).del();
            await this.recordEvent(trx, group_id, EventType.DELETE_LIST, username, item[0].title);
        });
    }

    async getGroups(user_id: number) {
        const ids = await knex("member").select("group_id").where({ user_id });
        return knex("group").select().whereIn("id", ids.map(m => m.group_id));
    }

    async getMembers(group_id: number) {
        return knex("member").select("user_id").where({ group_id });
    }

    async memberExists(group_id: number, user_id: number) {
        return (await knex("member").select().where({ group_id, user_id })).length > 0;
    }

    async getEvents(group_id: number, date: string) {
        const tomorrow = new Date(date);
        tomorrow.setDate(tomorrow.getDate() + 1);
        return knex("event").select("type", "by", "to", "created_at")
            .where({ group_id })
            .where("created_at", ">=", date)
            .where("created_at", "<", tomorrow);
    }

    async getLists(group_id: number) {
        return knex("list").select("id", "name", "description").where({ group_id });
    }

    async listIsInGroup(group_id: number, id: number) {
        return (await knex("list").select().where({ id, group_id })).length > 0;
    }

    async getItems(lists: number[]) {
        return knex("item").select().whereIn("list_id", lists);
    }

    async itemIsInGroup(group_id: number, id: number) {
        const item = await knex("item").select().where({ id });
        return !!item[0] && await this.listIsInGroup(group_id, item[0].list_id);
    }

    async completeItem(username: string, group_id: number, id: number) {
        return this.transaction(async trx => {
            const item = await trx("item").select("title").where({ id });
            await trx("item").where({ id }).update({ completed: true });
            await this.recordEvent(trx, group_id, EventType.COMPLETE_ITEM, username, item[0].title);
        });
    }

    async uncompleteItem(username: string, group_id: number, id: number) {
        return this.transaction(async trx => {
            const item = await trx("item").select("title").where({ id });
            await trx("item").where({ id }).update({ completed: false });
            await this.recordEvent(trx, group_id, EventType.UNCOMPLETE_ITEM, username, item[0].title);
        });
    }

}
