import { knex } from "../main";


export default class UserService {

    async getUserWithGmail(gmail: string) {
        return (await knex("user").select().where({ gmail }))[0];
    }

    async getUserWithId(id: number) {
        return (await knex("user").select().where({ id }))[0];
    }

    async getMembers(members: number[]) {
        return knex("user").select().whereIn("id", members);
    }

    async addUser(name: string, gmail: string, profile: string) {
        return knex("user").insert({ name: name.substr(0, 40), gmail, profile });
    }

}
